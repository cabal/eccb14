\begin{thebibliography}{}

\bibitem[Agresti(2002)Agresti]{Agresti02}
Agresti, A. (2002).
\newblock {\em Categorical Data Analysis (Wiley Series in Probability and
  Statistics)\/}.
\newblock Wiley series in probability and statistics. Wiley Interscience, 2
  edition.

\bibitem[Brinza {\em et~al.}(2010)Brinza, Schultz, Tesler, and Bafna]{Brinza10}
Brinza, D., Schultz, M., Tesler, G., and Bafna, V. (2010).
\newblock {RAPID} detection of gene-gene interactions in genome-wide
  association studies.
\newblock {\em Bioinformatics (Oxford, England)\/}, {\bf 26}(22), 2856--2862.

\bibitem[Canela-Xandri {\em et~al.}(2012)Canela-Xandri, Juli�, Gelp�, and
  Marsal]{CanelaXandri12}
Canela-Xandri, O., Juli�, A., Gelp�, J.~L., and Marsal, S. (2012).
\newblock Unveiling case-control relationships in designing a simple and
  powerful method for detecting gene-gene interactions.
\newblock {\em Genetic Epidemiology\/}, {\bf 36}(7), 710--716.

\bibitem[Chang(2014)Chang]{plink2}
Chang, C. (2014).
\newblock Plink 1.90.
\newblock \url{https://www.cog-genomics.org/plink2/}.

\bibitem[Chen {\em et~al.}(2009)Chen, Yu, Miller, Song, Langefeld, Herrington,
  Liu, and Wang]{Chen09}
Chen, L., Yu, G., Miller, D.~J., Song, L., Langefeld, C., Herrington, D., Liu,
  Y., and Wang, Y. (2009).
\newblock {A Ground Truth Based Comparative Study on Detecting Epistatic SNPs.}
\newblock {\em Proceedings. IEEE International Conference on Bioinformatics and
  Biomedicine\/}, {\bf 1-4}(Nov 2009), 26--31.

\bibitem[Cordell(2009)Cordell]{Cordell09}
Cordell, H.~J. (2009).
\newblock Detecting gene�gene interactions that underlie human diseases.
\newblock {\em Nature Reviews Genetics\/}, {\bf 10}(6), 392--404.

\bibitem[Culverhouse {\em et~al.}(2002)Culverhouse, Suarez, Lin, and
  Reich]{Culverhouse02}
Culverhouse, R., Suarez, B.~K., Lin, J., and Reich, T. (2002).
\newblock A perspective on epistasis: limits of models displaying no main
  effect.
\newblock {\em Am. J. Hum. Genet.}, {\bf 70}, 461�471.

\bibitem[Evans {\em et~al.}(2006)Evans, Marchini, Morris, and Cardon]{Evans06}
Evans, D.~M., Marchini, J., Morris, A.~P., and Cardon, L.~R. (2006).
\newblock {Two-Stage} {Two-Locus} models in {Genome-Wide} association.
\newblock {\em PLoS Genet\/}, {\bf 2}(9), e157+.

\bibitem[Fang {\em et~al.}(2012)Fang, Haznadar, Wang, Yu, Steinbach, Church,
  Oetting, {Van Ness}, and Kumar]{Fang12}
Fang, G., Haznadar, M., Wang, W., Yu, H., Steinbach, M., Church, T.~R.,
  Oetting, W.~S., {Van Ness}, B., and Kumar, V. (2012).
\newblock {High-order SNP combinations associated with complex diseases:
  efficient discovery, statistical power and functional interactions.}
\newblock {\em PloS ONE\/}, {\bf 7}(4).

\bibitem[Goudey {\em et~al.}(2013)Goudey, Rawlinson, Wang, Shi, Ferra,
  Campbell, Stern, Inouye, Ong, and Kowalczyk]{Goudey13}
Goudey, B., Rawlinson, D., Wang, Q., Shi, F., Ferra, H., Campbell, R., Stern,
  L., Inouye, M., Ong, C.~S., and Kowalczyk, A. (2013).
\newblock Gwis - model-free, fast and exhaustive search for epistatic
  interactions in case-control gwas.
\newblock {\em BMC Genomics\/}, {\bf 14}(Suppl 3), S10.

\bibitem[Greene {\em et~al.}(2010)Greene, Sinnott-Armstrong, Himmelstein, Park,
  Moore, and Harris]{Greene10}
Greene, C.~S., Sinnott-Armstrong, N.~A., Himmelstein, D.~S., Park, P.~J.,
  Moore, J.~H., and Harris, B.~T. (2010).
\newblock Multifactor dimensionality reduction for graphics processing units
  enables genome-wide testing of epistasis in sporadic {ALS}.
\newblock {\em Bioinformatics (Oxford, England)\/}, {\bf 26}(5), 694--695.

\bibitem[Gyenesei {\em et~al.}(2012)Gyenesei, Moody, Semple, Haley, and
  Wei]{Gyenesei12}
Gyenesei, A., Moody, J., Semple, C.~A., Haley, C.~S., and Wei, W.-H.~H. (2012).
\newblock High-throughput analysis of epistasis in genome-wide association
  studies with {BiForce}.
\newblock {\em Bioinformatics (Oxford, England)\/}, {\bf 28}(15), 1957--1964.

\bibitem[Hirschhorn and Daly(2005)Hirschhorn and Daly]{Hirschhorn05}
Hirschhorn, J.~N. and Daly, M.~J. (2005).
\newblock Genome-wide association studies for common diseases and complex
  traits.
\newblock {\em Nat Rev Genet\/}, {\bf 6}(2), 95--108.

\bibitem[Hoh {\em et~al.}(2000)Hoh, Wille, Zee, Cheng, Reynolds, Lindpaintner,
  and Ott]{Hoh00}
Hoh, J., Wille, A., Zee, R., Cheng, S., Reynolds, R., Lindpaintner, K., and
  Ott, J. (2000).
\newblock Selecting snps in two-stage analysis of disease association data: a
  model-free approach.
\newblock {\em Annals of Human Genetics\/}, {\bf 64}(5), 413--417.

\bibitem[Hu {\em et~al.}(2013)Hu, Chen, Kiralis, Collins, Wejse, Sirugo,
  Williams, and Moore]{Hu13}
Hu, T., Chen, Y., Kiralis, J.~W., Collins, R.~L., Wejse, C., Sirugo, G.,
  Williams, S.~M., and Moore, J.~H. (2013).
\newblock An information-gain approach to detecting three-way epistatic
  interactions in genetic association studies.
\newblock {\em Journal of the American Medical Informatics Association :
  JAMIA\/}, {\bf 20}(4), 630--636.

\bibitem[Hu {\em et~al.}(2010)Hu, Liu, Zhang, Li, Wang, He, and Shi]{Hu10}
Hu, X., Liu, Q., Zhang, Z., Li, Z., Wang, S., He, L., and Shi, Y. (2010).
\newblock {SHEsisEpi}, a {GPU}-enhanced genome-wide {SNP}-{SNP} interaction
  scanning algorithm, efficiently reveals the risk genetic epistasis in bipolar
  disorder.
\newblock {\em Cell Research\/}, {\bf 20}(7), 854--857.

\bibitem[Kam-Thong {\em et~al.}(2011)Kam-Thong, Czamara, Tsuda, Borgwardt,
  Lewis, Erhardt-Lehmann, Hemmer, Rieckmann, Daake, Weber, Wolf, Ziegler,
  P\"{u}tz, Holsboer, Sch\"{o}lkopf, and M\"{u}ller-Myhsok]{Kam11}
Kam-Thong, T., Czamara, D., Tsuda, K., Borgwardt, K., Lewis, C.~M.,
  Erhardt-Lehmann, A., Hemmer, B., Rieckmann, P., Daake, M., Weber, F., Wolf,
  C., Ziegler, A., P\"{u}tz, B., Holsboer, F., Sch\"{o}lkopf, B., and
  M\"{u}ller-Myhsok, B. (2011).
\newblock {EPIBLASTER}-fast exhaustive two-locus epistasis detection strategy
  using graphical processing units.
\newblock {\em European Journal of Human Genetics\/}, {\bf 19}, 465--471.

\bibitem[Lewis and Knight(2012)Lewis and Knight]{Lewis12}
Lewis, C.~M. and Knight, J. (2012).
\newblock {Introduction to genetic association studies.}
\newblock {\em Cold Spring Harbor protocols\/}, {\bf 2012}(3), 297--306.

\bibitem[Marchini {\em et~al.}(2005)Marchini, Donnelly, and Cardon]{Marchini05}
Marchini, J., Donnelly, P., and Cardon, L.~R. (2005).
\newblock Genome-wide strategies for detecting multiple loci that influence
  complex diseases.
\newblock {\em Nat Genet\/}, {\bf 37}(4), 413--417.

\bibitem[Millstein {\em et~al.}(2006)Millstein, Conti, Gilliland, and
  Gauderman]{Millstein06}
Millstein, J., Conti, D.~V., Gilliland, F.~D., and Gauderman, W.~J. (2006).
\newblock A testing framework for identifying susceptibility genes in the
  presence of epistasis.
\newblock {\em American journal of human genetics\/}, {\bf 78}(1), 15--27.

\bibitem[Moore and Williams(2002)Moore and Williams]{Moore02}
Moore, J.~H. and Williams, S.~M. (2002).
\newblock New strategies for identifying gene-gene interactions in
  hypertension.
\newblock {\em Annals of Medicine\/}, {\bf 34}(2), 88--95.

\bibitem[Prabhu and Pe'er(2012)Prabhu and Pe'er]{Prabhu12}
Prabhu, S. and Pe'er, I. (2012).
\newblock {Ultrafast genome-wide scan for SNP-SNP interactions in common
  complex disease.}
\newblock {\em Genome Res\/}, {\bf 22}, 2230--2240.

\bibitem[Purcell(2007)Purcell]{plink}
Purcell, S. (2007).
\newblock Plink 1.07.
\newblock \url{http://pngu.mgh.harvard.edu/purcell/plink/}.

\bibitem[Purcell {\em et~al.}(2007)Purcell, Neale, Todd-Brown, Thomas,
  Ferreira, Bender, Maller, Sklar, de~Bakker, Daly, and Sham]{Purcell07}
Purcell, S., Neale, B., Todd-Brown, K., Thomas, L., Ferreira, M.~A., Bender,
  D., Maller, J., Sklar, P., de~Bakker, P.~I., Daly, M.~J., and Sham, P.~C.
  (2007).
\newblock {PLINK}: a tool set for whole-genome association and population-based
  linkage analyses.
\newblock {\em American journal of human genetics\/}, {\bf 81}(3), 559--575.

\bibitem[Ritchie {\em et~al.}(2001)Ritchie, Hahn, Roodi, Bailey, Dupont, Parl,
  and Moore]{Ritchie01}
Ritchie, M.~D., Hahn, L.~W., Roodi, N., Bailey, L.~R., Dupont, W.~D., Parl,
  F.~F., and Moore, J.~H. (2001).
\newblock Multifactor-dimensionality reduction reveals high-order interactions
  among estrogen-metabolism genes in sporadic breast cancer.
\newblock {\em American journal of human genetics\/}, {\bf 69}(1), 138--147.

\bibitem[Wan {\em et~al.}(2010)Wan, Yang, Yang, Xue, Fan, Tang, and Yu]{Wan10}
Wan, X., Yang, C., Yang, Q., Xue, H., Fan, X., Tang, N.~L., and Yu, W. (2010).
\newblock Boost: A fast approach to detecting gene-gene interactions in
  genome-wide case-control studies.
\newblock {\em The American Journal of Human Genetics\/}, {\bf 87}(3), 325 --
  340.

\bibitem[Wang {\em et~al.}(2011a)Wang, Liu, Feng, and Wong]{Wang11a}
Wang, Y., Liu, G., Feng, M., and Wong, L. (2011a).
\newblock An empirical comparison of several recent epistatic interaction
  detection methods.
\newblock {\em Bioinformatics\/}, {\bf 27}(21), 2936--2943.

\bibitem[Wang {\em et~al.}(2011b)Wang, Wang, Tan, Wong, and Agrawal]{Wang11b}
Wang, Z., Wang, Y., Tan, K.-L.~L., Wong, L., and Agrawal, D. (2011b).
\newblock {eCEO}: an efficient cloud epistasis {cOmputing} model in genome-wide
  association study.
\newblock {\em Bioinformatics (Oxford, England)\/}, {\bf 27}(8), 1045--1051.

\bibitem[Wei {\em et~al.}(2009)Wei, Knott, Haley, and de~Koning]{Wei10}
Wei, W.~H., Knott, S., Haley, C.~S., and de~Koning, D.~J. (2009).
\newblock Controlling false positives in the mapping of epistatic {QTL}.
\newblock {\em Heredity\/}, {\bf 104}, 401�409.

\bibitem[Wilson {\em et~al.}(2014)Wilson, Aruliah, Brown, Chue~Hong, Davis,
  Guy, Haddock, Huff, Mitchell, Plumbley, Waugh, White, and
  Wilson]{Wilson2014PlosBiol}
Wilson, G., Aruliah, D.~A., Brown, C.~T., Chue~Hong, N.~P., Davis, M., Guy,
  R.~T., Haddock, S. H.~D., Huff, K.~D., Mitchell, I.~M., Plumbley, M.~D.,
  Waugh, B., White, E.~P., and Wilson, P. (2014).
\newblock Best practices for scientific computing.
\newblock {\em PLoS Biol\/}, {\bf 12}(1), e1001745.

\bibitem[Wu {\em et~al.}(2010)Wu, Devlin, Ringquist, Trucco, and Roeder]{Wu10}
Wu, J., Devlin, B., Ringquist, S., Trucco, M., and Roeder, K. (2010).
\newblock Screen and clean: a tool for identifying interactions in genome-wide
  association studies.
\newblock {\em Genetic Epidemiology\/}, {\bf 34}(3), 275--285.

\bibitem[Yang {\em et~al.}(2009)Yang, He, Wan, Yang, Xue, and Yu]{Yang09}
Yang, C., He, Z., Wan, X., Yang, Q., Xue, H., and Yu, W. (2009).
\newblock {SNPHarvester}: a filtering-based approach for detecting epistatic
  interactions in genome-wide association studies.
\newblock {\em Bioinformatics (Oxford, England)\/}, {\bf 25}(4), 504--511.

\bibitem[Zhang {\em et~al.}(2010a)Zhang, Pan, Xie, Zou, and Wang]{Zhang09}
Zhang, X., Pan, F., Xie, Y., Zou, F., and Wang, W. (2010a).
\newblock {COE}: a general approach for efficient genome-wide two-locus
  epistasis test in disease association study.
\newblock {\em Journal of computational biology : a journal of computational
  molecular cell biology\/}, {\bf 17}(3), 401--415.

\bibitem[Zhang {\em et~al.}(2010b)Zhang, Huang, Zou, and Wang]{Zhang10}
Zhang, X., Huang, S., Zou, F., and Wang, W. (2010b).
\newblock Team: efficient two-locus epistasis tests in human genome-wide
  association study.
\newblock {\em Bioinformatics\/}, {\bf 26}(12), i217--i227.

\bibitem[Zhang and Liu(2007)Zhang and Liu]{Zhang07}
Zhang, Y. and Liu, J.~S. (2007).
\newblock Bayesian inference of epistatic interactions in case-control studies.
\newblock {\em Nature Genetics\/}, {\bf 39}(9), 1167--1173.

\bibitem[Ziegler {\em et~al.}(2008)Ziegler, K�nig, and Thompson]{Ziegler08}
Ziegler, A., K�nig, I.~R., and Thompson, J.~R. (2008).
\newblock Biostatistical aspects of genome-wide association studies.
\newblock {\em Biometrical Journal\/}, {\bf 50}(1), 8--28.

\end{thebibliography}
